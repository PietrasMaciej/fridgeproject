﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Fridge.Validations;

namespace Fridge.Models
{
    public class Product
    {
        public int ProductId { get; set; }
        [Required]
        [Display(Name = "Produkt")]
        public string Name { get; set; }
        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Data ważności")]
        [ExpirationDate]
        public DateTime Date { get; set; }
        [Display(Name = "Dodatkowe informacje")]
        public string Comment { get; set; }
        public int FridgesID { get; set; }

        public virtual Fridges Fridges { get; set; }
    }
}