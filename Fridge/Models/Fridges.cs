﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Fridge.Models
{
    public class Fridges
    {
        public int FridgesId { get; set; }
        [Required]
        [Display(Name = "Położenie")]
        public string Location { get; set; }
        [Display(Name = "Marka")]
        public string Brand { get; set; }
        [Display(Name = "Kolor")]
        public string Color { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}