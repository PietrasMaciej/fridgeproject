﻿function timeFormat(h, m, s) {
    return h + ":" + m + ":" + s;
}

function timeClock() {
    setTimeout(timeClock, 1000);
    now = new Date();
    f_date = now.getDate() + "." + now.getMonth() + 1 + "." + now.getFullYear() + "&nbsp;&nbsp;&nbsp;&nbsp;" +
        timeFormat(now.getHours(), ("0" + now.getMinutes()).slice(-2), ("0" + now.getSeconds()).slice(-2));
    document.getElementById("currTime").innerHTML = f_date;

}

$(function () {
    timeClock();
});