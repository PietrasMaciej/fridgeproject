﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Fridge.Validations
{
    public class ExpirationDateAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            DateTime expirationDate = Convert.ToDateTime(value);

            return expirationDate >= DateTime.Now;

            /*
            if (expirationDate >= DateTime.Now)
            {
                return true;
            }
            else
            {
                return false;
            }
            */
        }
    }
}