﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Fridge.Startup))]
namespace Fridge
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
