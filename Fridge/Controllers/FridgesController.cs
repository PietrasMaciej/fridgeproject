﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Fridge.Models;

namespace Fridge.Controllers
{
    public class FridgesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Fridges
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View(db.Fridges.ToList());
        }

        // GET: Fridges/Details/5
        [Route("Products {id}")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Fridges fridges = db.Fridges.Find(id);
            if (fridges == null)
            {
                return HttpNotFound();
            }
            return View(fridges);
        }

        // GET: Fridges/Create
        [Authorize(Roles = "Administrator")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Fridges/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public ActionResult Create([Bind(Include = "FridgesId,Location,Brand,Color")] Fridges fridges)
        {
            if (ModelState.IsValid)
            {
                db.Fridges.Add(fridges);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(fridges);
        }

        // GET: Fridges/Edit/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Fridges fridges = db.Fridges.Find(id);
            if (fridges == null)
            {
                return HttpNotFound();
            }
            return View(fridges);
        }

        // POST: Fridges/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public ActionResult Edit([Bind(Include = "FridgesId,Location,Brand,Color")] Fridges fridges)
        {
            if (ModelState.IsValid)
            {
                db.Entry(fridges).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(fridges);
        }

        // GET: Fridges/Delete/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Fridges fridges = db.Fridges.Find(id);
            if (fridges == null)
            {
                return HttpNotFound();
            }
            return View(fridges);
        }

        // POST: Fridges/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public ActionResult DeleteConfirmed(int id)
        {
            Fridges fridges = db.Fridges.Find(id);
            db.Fridges.Remove(fridges);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
