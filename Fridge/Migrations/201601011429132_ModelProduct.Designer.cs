// <auto-generated />
namespace Fridge.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class ModelProduct : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ModelProduct));
        
        string IMigrationMetadata.Id
        {
            get { return "201601011429132_ModelProduct"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
