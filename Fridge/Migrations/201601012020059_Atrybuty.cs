namespace Fridge.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Atrybuty : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Fridges", "Location", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Fridges", "Location", c => c.String());
        }
    }
}
