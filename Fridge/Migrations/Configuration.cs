namespace Fridge.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Fridge.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;

    internal sealed class Configuration : DbMigrationsConfiguration<Fridge.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "Fridge.Models.ApplicationDbContext";
        }

        bool AddUserAndRole(Fridge.Models.ApplicationDbContext context)
        {
            IdentityResult ir;
            var rm = new RoleManager<IdentityRole>
                (new RoleStore<IdentityRole>(context));
            ir = rm.Create(new IdentityRole("Administrator"));
            var um = new UserManager<ApplicationUser>(
                new UserStore<ApplicationUser>(context));
            var user = new ApplicationUser()
            {
                UserName = "admin@fridge.project.pl",
            };
            ir = um.Create(user, "Qwerty1!");
            if (ir.Succeeded == false)
                return ir.Succeeded;
            ir = um.AddToRole(user.Id, "Administrator");
            return ir.Succeeded;
        }

        protected override void Seed(Fridge.Models.ApplicationDbContext context)
        {
            AddUserAndRole(context);
            context.Fridges.AddOrUpdate(p => p.Location,
      new Fridges
      {
          Location = "Kuchnia",
          Brand = "LG",
          Color = "Stalowy",
      },
       new Fridges
       {
           Location = "Spi�arnia",
           Brand = "Samsung",
           Color = "Srebrny",
       },
       new Fridges
       {
           Location = "Gara�",
           Brand = "Amica",
           Color = "Bia�y",
       }
       );
        }
    }
}
