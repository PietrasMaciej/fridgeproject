namespace Fridge.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModelProduct : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ProductId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Date = c.DateTime(nullable: false),
                        Comment = c.String(),
                        FridgesID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ProductId)
                .ForeignKey("dbo.Fridges", t => t.FridgesID, cascadeDelete: true)
                .Index(t => t.FridgesID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "FridgesID", "dbo.Fridges");
            DropIndex("dbo.Products", new[] { "FridgesID" });
            DropTable("dbo.Products");
        }
    }
}
